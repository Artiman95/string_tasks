text = input("Enter text: ")
words = text.split()
new_words = []

for word in words:
    if len(word) > 2:
        new_word = word[0].upper() + word[1:-1] + word[-1].upper()
    else:
        new_word = word
    new_words.append(new_word)

new_text = ' '.join(new_words)
print(new_text)

# string for example: This is a test string This string contains words
