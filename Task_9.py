def conversation_in_percent(number):
    result = '{:.2%}'.format(number)
    return result


enter_number = float(input('Enter number: '))
print(conversation_in_percent(enter_number))
