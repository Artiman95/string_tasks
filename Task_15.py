import re


def count_chars(text):
    upper = len(re.findall(r'[A-ZА-Я]', text))
    lower = len(re.findall(r'[a-zа-я]', text))
    digits = len(re.findall(r'\d', text))
    special = len(re.findall(r'[^\w\s]', text))
    return upper, lower, digits, special


enter_text = input('Enter string with spaces: ')
upper, lower, digits, special = count_chars(enter_text)

print('Number of capital letters ', upper)
print('Number of lowercase letters: ', lower)
print('Number of digits: ', digits)
print('Number of special characters: ', special)
