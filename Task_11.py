def counter_words_in_text(text):
    list_word = text.split()
    count = {}
    for element in list_word:
        if count.get(element, None):
            count[element] += 1
        else:
            count[element] = 1
    sorted_values = sorted(count.items(), key=lambda x: x[1], reverse=True)
    dict_values = dict(sorted_values)
    return dict_values


enter_text = str(input('Enter string with spaces: '))
print(counter_words_in_text(enter_text))

# string for example: This is a test string This string contains words
