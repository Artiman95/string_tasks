string1 = 'Python3'
string2 = 'Python2.7'
output = ''

for i in range(min(len(string1), len(string2))):
    if string1[i] == string2[i]:
        output += string1[i]
    else:
        break

print(output)
