from collections import Counter


def find_second_frequent_word(text):
    words = text.split()
    word_count = Counter(words)
    most_common = word_count.most_common(2)
    if len(most_common) < 2 or most_common[0][1] != most_common[1][1]:
        return None
    return most_common[1][0]


enter_line = str(input('Enter string with spaces: '))

most_freq_word = find_second_frequent_word(enter_line)
print(most_freq_word)

# string for example: This is a test string This string contains words
