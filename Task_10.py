def reverse_text(line):
    if len(line) % 4 == 0:
        reverse_line = ''.join(reversed(line))
        return reverse_line
    return line


enter_line = str(input('Enter word: '))
print(reverse_text(enter_line))

