def extracting_numbers(string):
    numbers = []
    current_number = ''
    for char in string:
        if char.isdigit():
            current_number += char
        elif current_number:
            numbers.append(int(current_number))
            current_number = ''
    if current_number:
        numbers.append(int(current_number))
    return numbers


example_string = input('Enter string: ')
list_conversion = list(example_string)
print(extracting_numbers(list_conversion))
