def remove_duplicates(text):
    unique_chars = ''
    for char in text:
        if char not in unique_chars:
            unique_chars += char
    return unique_chars


enter_text = input('Enter string with spaces: ')
print(remove_duplicates(enter_text))
