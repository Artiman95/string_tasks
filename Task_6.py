from collections import Counter


def find_most_frequent_word(text):
    words = text.split()
    word_count = Counter(words)
    return max(word_count, key=word_count.get)


enter_line = str(input('Enter string with spaces: '))

most_freq_word = find_most_frequent_word(enter_line)
print(most_freq_word)

# string for example: This is a test string This string contains words
