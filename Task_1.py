def sum_string_numbers(a, b):
    if not (a and b):
        return False
    elif not (a.isdigit() and b.isdigit()):
        return False
    else:
        return str(int(a) + int(b))


num1 = '234242342341'
num2 = '2432342342'
print(sum_string_numbers(num1, num2))

num3 = ''
num4 = '2432342342'
print(sum_string_numbers(num3, num4))

num5 = '1000'
num6 = '0'
print(sum_string_numbers(num5, num6))

num7 = '1000'
num8 = '10'
print(sum_string_numbers(num7, num8))

num9 = 'Hello world!'
num10 = '10'
print(sum_string_numbers(num9, num10))
