def remove_spaces(string):
    return string.replace(" ", "")


enter_string = str(input('Enter string with spaces: '))

string_without_spaces = remove_spaces(enter_string)
print(string_without_spaces)
