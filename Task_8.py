def reverse_text(text):
    words = text.split()
    reversed_text = ' '.join(reversed(words))
    return reversed_text


enter_line = str(input('Enter string with spaces: '))
print(reverse_text(enter_line))

# string for example: This is a test string This string contains words
