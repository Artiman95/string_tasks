def add_space_before_capital_letters(s):
    result = ""
    for c in s:
        if c.isupper():
            result += ' '
        result += c
    return result


word1 = 'PythonExercises'
word2 = 'Python'
word3 = 'PythonExercisesPracticeSolution'

print(add_space_before_capital_letters(word1))
print(add_space_before_capital_letters(word2))
print(add_space_before_capital_letters(word3))
